import pytest
from pytest_f3ts.utils import log_vars

def test_voltage(test_config, record_property):

    meas = 3.3

    assert meas > test_config.min_limit, "Voltage too low"
    assert meas < test_config.max_limit, "Voltage too high"
    log_vars(record_property)

def test_program(test_config, record_property):
    meas = True
    assert meas, test_config.error_msg
    log_vars(record_property)


#
# # Add the `record_property` fixture to the test case
# def test_5v0_voltage_rail(record_property):
#     # Set test limits as variables and add them to the test log
#     min_limit = 4.9
#     max_limit = 5.1
#
#     # Measured voltage and add to the test log
#     meas = get_5v0_voltage()
#
#     # Call log_vars prior to any assertions
#     log_vars(record_property)
#
#     # Check against a low and high limit
#     assert meas > min_limit, "Voltage too low"
#     assert meas < max_limit, "Voltage too high"